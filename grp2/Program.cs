﻿using System;

namespace grp2
{
    class Program
    {
        static void Main(string[] args)
        {
            // b is the number, sum is the total amount
            int sum = 0;
            int b = 0;
            
            //wording 
            Console.WriteLine("Please enter a number: ");
            //input
            int a = Convert.ToInt32(Console.ReadLine());
            // allow a user to enter a number and the program will print out the natural numbers 1 to that number.
            while (b < a)
            {
                b++;
                Console.WriteLine(b);
                sum += b;
            }

            Console.WriteLine("The sum of the numbers is: " + sum);

        }
    }
}
